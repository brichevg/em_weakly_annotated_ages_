Initial setup:
- git clone https://gitlab.fel.cvut.cz/brichevg/em_weakly_annotated_ages_.git
- cd data
- unzip AgeGenderFaces.zip

run_age_prediction.py script for training or testing the cnn in the supervised way:
Parameters:

--model_name - which model to test or under which name to save the trained model

--only_test - flag, that specifies only testing puposes

--lr - learning rate (only for training)

--max_mae - cnn stops training when this MAE achieved in validation


Test initial supervised pretained model, used in EM(gives MAE of 10):

`python3 run_age_prediction.py --model_name 'initial_model' --only_test`

Test initial supervised fully trained model(gives MAE of 4.3):

`python3 run_age_prediction.py --model_name 'fully_trained' --only_test`

Test trained model by EM (gives MAE of 4.7):

`python3 run_age_prediction.py --model_name 'final_model_cuda' --only_test`

Run EM:

`python3 main.py`

To clarify the logs:
After each epoch of EM, if the model is updated (if validation loss decreased), the MAE of test data is calculated to monitor the change of the model.

Pretrain the initial model in supervised way (for the EM usage, I put max MAE 10):

`python3 run_age_prediction.py --model_name 'initial_model' --lr 0.00001 --max_mae 10`

Train the model in supervised way:

`python3 run_age_prediction.py --model_name 'fully_trained' --lr 0.01 --max_mae 0`
