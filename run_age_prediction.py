import cnn_generator
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--model_name", type=str)
parser.add_argument("--only_test", action='store_true')
parser.add_argument("--lr", default=None, type=float)
parser.add_argument("--max_mae", default=None, type=float)
args = parser.parse_args()
cnn_generator.train_agedb(model_name=args.model_name, only_test=args.only_test, lr=args.lr, max_mae=args.max_mae)
