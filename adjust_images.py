import itertools
from csv import reader
import cv2 as cv
import os
import numpy as np


def adjust(path, landmarks):
    img = cv.imread(os.path.join('data/', path))
    margin = (landmarks[1, 0] - landmarks[0, 0]) * 0.125

    new_landmarks = [landmarks[0, 0] - margin,
                     landmarks[1, 0] + margin,
                     landmarks[0, 1] - margin,
                     landmarks[2, 1] + margin]
    border_size = 0
    if min(new_landmarks) < 0:
        border_size = np.round(abs(min(new_landmarks))).astype(int)
        new_landmarks = [new_landmarks[0] + border_size,
                         new_landmarks[1] - border_size,
                         new_landmarks[2] + border_size,
                         new_landmarks[3] - border_size]
    new_landmarks = np.array(new_landmarks).astype(int)
    crop_img = img[new_landmarks[0]:new_landmarks[1], new_landmarks[2]:new_landmarks[3]]
    with_border = cv.copyMakeBorder(crop_img, border_size, border_size, border_size, border_size,
                                    cv.BORDER_CONSTANT)
    small_img = cv.resize(with_border, (64, 64))

    # greyscale
    gray = cv.cvtColor(small_img, cv.COLOR_BGR2GRAY)

    return gray


def get_landmarks(features):
    landmarks = features[1:9]
    landmarks = np.asarray(landmarks)
    landmarks = landmarks.astype('int').reshape(-1, 2)
    return landmarks


def prepare_images(source_feature_path="features/morph_bags1.csv",
                   abs_destination_img_path='some_directory'):
    try:
        os.mkdir(abs_destination_img_path)
        print("Directory ", abs_destination_img_path, " created ")
        print('preparing data...')
        with open(source_feature_path, 'r') as read_obj:
            data = reader(read_obj)
            for features in itertools.islice(data, 1, None):
                img_name = features[0].split('/')[-1]
                img = adjust(features[0], get_landmarks(features))
                cv.imwrite(os.path.join(abs_destination_img_path, img_name), img)
        print('finished')
    except FileExistsError:
        print("Directory ", abs_destination_img_path, " already exists")


# run python3 adjust_images
prepare_images()
